# [how-npm-am-i.js](https://git.coolaj86.com/coolaj86/how-npm-am-i.js)

Shows how many downloads you get on npm each month, across all of your packages.

> npx how-npm-am-i &lt;username> [--verbose]

# Usage

```bash
how-npm-am-i isaacs --verbose
```

```txt
You've published 161 packages to npm and you get...

Package Name                             Downloads
@isaacs/example-automatic-publishing:            3
cluster-callresp:                               11
voxer-blog-demo:                                12
tako-session-token:                             13
truncating-stream:                              18
...
rimraf:                                 59,309,683
minimatch:                              63,109,081
yallist:                                66,527,785
lru-cache:                              77,627,756
glob:                                  109,654,115

1,423,623,087 downloads per month. Not bad!
You're *very* npm.
```

# Install

```bash
npm install --global how-npm-am-i
```

# How I built this

See <https://coolaj86.com/articles/how-cool-you-are-on-npm/>