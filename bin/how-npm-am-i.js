#!/usr/bin/env node
'use strict';

var pkg = require('../package.json');

var args = process.argv.slice(1).join(',');
if (/-V|--version/.test(args)) {
	console.info('v' + pkg.version);
	return;
}

var verbose = false;
if (/-v|--verbose/.test(args)) {
	verbose = true;
}

var username = process.argv[2];
if (!username) {
	console.error();
	console.error('Usage: how-npm-am-i <username>');
	console.error('   Ex: how-npm-am-i isaacs');
	console.error();
	process.exit(1);
	return;
}

var hnai = require('../');
hnai
	.packages(username)
	.then(function(pkgnames) {
		console.info('');
		if (!pkgnames.length) {
			console.info("You've published " + pkgnames.length + ' packages to npm. Hmm.');
			console.info('Well, we all have to start somewhere.... except maybe you.... you just... keep doing you. 👍');
			console.info('');
			process.exit(1337);
			return;
		}
		console.info("You've published " + pkgnames.length + ' packages to npm and you get...');
		hnai.downloads(pkgnames).then(function(downloads) {
			var total = downloads.reduce(function(sum, el) {
				return sum + el.downloads;
			}, 0);
			if (verbose) {
				tabularize(downloads);
			}
			console.info(total.toLocaleString() + ' downloads per month. Not bad!');
			if (total >= 100000 || pkgnames.length > 100) {
				console.info("You're a little npm. Keep trying.");
			} else {
				console.info("You're *very* npm. Much wow!");
			}
			console.info();
		});
	})
	.catch(function(e) {
		console.error();
		console.error(e);
		console.error();
		process.exit(1);
	});

function tabularize(downloads) {
	console.info('');
	var maxLen = 0;
	var maxCount = "Downloads".length;
	downloads.sort(function(a, b) {
		maxLen = Math.max(a.package.length, b.package.length, maxLen);
		maxCount = Math.max(a.downloads.toLocaleString().length, b.downloads.toLocaleString().length, maxCount);
		return a.downloads - b.downloads;
	});
	console.info(leftpad('Package Name', maxLen + 1), rightmad('Downloads', maxCount + 1));
	downloads.forEach(function(stat) {
		console.info(leftpad(stat.package + ':', maxLen + 1), rightmad(stat.downloads.toLocaleString(), maxCount + 1));
	});
	console.info('');
}

// no, I was right
function leftpad(str, n) {
	while (str.length < n) {
		str = str + ' ';
	}
	return str;
}

function rightmad(str, n) {
	while (str.length < n) {
		str = ' ' + str;
	}
	return str;
}
