'use strict';

var hnai = require('./');
hnai.packages('coolaj86').then(function(pkgnames) {
	console.info('Total number of pakcages:', pkgnames.length);
	hnai.downloads(pkgnames).then(function(downloads) {
		console.log(
			'Download Counts:',
			downloads
				.map(function(all) {
					return all.package + ' ' + all.downloads;
				})
				.join('\n')
		);
	});
});
